# YouTube Minetest Stream Chat

Usage:

* copy `ytmc_mod` to minetest mods directory
* disable mod security
* `cd server`
* `pipenv install`
* `pipenv run python server.py <stream_id> <log_path>`

`log_path` example: /home/snegg21/.minetest/mods/ytmc_mod/ytmchat.log

## License

GNU GPL v3
