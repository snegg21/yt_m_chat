local open = io.open

local path_self = minetest.get_modpath(minetest.get_current_modname())
local path_sep = package.path:match('(%p)%?%.')

local cache = 0

local function read_file(path)
    local file = open(path)
    if not file then return nil end
    lines = file:lines()
    local count = 0
    for line in lines do
        if cache <= count then
            print(line)
            minetest.chat_send_all(line)
            cache = cache + 1
        end
        count = count + 1
    end
    file:close()
end

minetest.register_globalstep(
    function(dtime)
        read_file(path_self .. path_sep .. "ytmchat.log")
    end
)
