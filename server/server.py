import os
import sys

import pytchat

if len(sys.argv) < 3:
    print("no arguments")
    sys.exit(2)
STREAM_ID = sys.argv[1]
PATH_LOG = sys.argv[2]
if os.path.exists(PATH_LOG):
    os.remove(PATH_LOG)
chat = pytchat.create(video_id=STREAM_ID)
while chat.is_alive():
    for c in chat.get().sync_items():
        print(f"{c.datetime} [{c.author.name}]: {c.message}")
        with open(PATH_LOG, "a", encoding="utf8") as logfile:
            logfile.write(f"[YT] [{c.author.name}]: {c.message}")
            logfile.write(os.linesep)
